package com.meshservice.gateway.books.contoller;

import com.meshservice.gateway.books.clients.BooksClient;
import com.meshservice.gateway.books.model.BookDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class BooksController {

    @Autowired
    private BooksClient booksClient;

    @CrossOrigin
    @GetMapping(value = "/books")
    public List<BookDto> getAllBooks() {
        return this.booksClient.getAllBooks();
    }
}
