package com.meshservice.gateway.books.clients;

import com.meshservice.gateway.books.model.BookDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@FeignClient(name="books", url="books")
//@FeignClient(value = "books-ws", url = "http://localhost:8081")
public interface BooksClient {

    @RequestMapping(method = RequestMethod.GET, value = "/books")
    List<BookDto> getAllBooks();
}
